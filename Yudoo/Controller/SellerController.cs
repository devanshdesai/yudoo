﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.Helpers;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;

namespace Yudoo.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private readonly ISellerServices _SellerService;
        public SellerController(ISellerServices SellerService)
        {
            _SellerService = SellerService;
        }


        [HttpGet]
        [ActionName("customer_otp")]
        public async Task<ActionResult> GetRegisteredShopDetails(string name, string shop_name, string mobile,bool is_register)
        {
            try
            {
                var result = await _SellerService.GetRegisteredShopDetails(name, shop_name, mobile, is_register);
                if (result)
                {
                    return Ok(new ResponseObject<int>
                    {
                        Status = "ok",
                        Data = ConversionHelper.GenerateOtp()
                    });
                }
                else
                {
                    throw new NotFoundException("Candidate Not Found");
                }

            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
