﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Repositories;

namespace Yudoo.Infrastructure.Services
{

    public interface ISellerServices
    {
        Task<bool> GetRegisteredShopDetails(string name, string shop_name, string mobile,bool is_register);
    }
    public class SellerServices : ISellerServices
    {
        private readonly ISellerRepositories _sellerRepository;

        public SellerServices(ISellerRepositories sellerRepository)
        {
            _sellerRepository = sellerRepository;
        }

        public async Task<bool> GetRegisteredShopDetails(string name, string shop_name, string mobile,bool is_register)
        {
            var exists = await _sellerRepository.GetRegisteredShopDetails(name,shop_name,mobile);
            if (is_register)
            {
                if (exists)
                    return true;                
                else
                    throw new NotFoundException("shop not exists.");
                
            }
            else
            {
                if (exists)
                {
                    throw new NotFoundException("Mobile number already exists.");
                }
                else
                {
                    await _sellerRepository.AddUpdateShopOwner(0, shop_name, name, null, null, null, null, null, null, null, null, null, null, null, null);
                    return false;
                }
            }
            
        }
    }
}
