﻿using System;

namespace Hippo.InternalApp.Infrastructure.Common.Exceptions
{
    public class ServiceBusException : Exception
    {
        public ServiceBusException(string message, Exception ex)
            : base(message, ex) { }
    }
}
