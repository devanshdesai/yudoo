﻿using System;

namespace Hippo.InternalApp.Infrastructure.Common.Exceptions
{
    public class DeadLockException : Exception
    {
        public DeadLockException(string message)
            : base(message)
        {

        }

    }
}
