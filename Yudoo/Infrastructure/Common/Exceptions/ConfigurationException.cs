﻿using System;

namespace Hippo.InternalApp.Infrastructure.Common.Exceptions
{
    public class ConfigurationException : Exception
    {
        public ConfigurationException(string message)
            : base(message) { }
    }
}
