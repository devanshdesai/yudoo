﻿using System;

namespace Yudoo.Infrastructure.Common.Helpers
{
    public class ConversionHelper
    {
        private string GenerateRandomOTP(int iOTPLength, string[] saAllowedCharacters)
        {
            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();

            for (int i = 0; i < iOTPLength; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }
            return sOTP;
        }

        public static int GenerateOtp()
        {
            try
            {
                int min = 1000;
                int max = 9999;
                int otp = 0;

                Random rdm = new Random();
                otp = rdm.Next(min, max);
                return otp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
