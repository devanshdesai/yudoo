﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Yudoo.Infrastructure.Repositories
{
    public abstract class BaseRepository
    {
        private readonly string _connectionString;

        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }


        public async Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                try
                {
                    return await conn.QueryAsync<T>(sql, param, commandType: CommandType.StoredProcedure);
                }
                catch (SqlException ex) when (ex.Number == 1205)
                {
                    throw new DeadLockException(ex.Message);
                }
                catch (SqlException ex) when (ex.Number == -2)
                {
                    throw new DatabaseTimeoutException(ex.Message);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async Task ExecuteAsync(string sql, object param = null)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                try
                {
                    await conn.ExecuteAsync(sql, param, commandType: CommandType.StoredProcedure);
                }
                catch (SqlException ex) when (ex.Number == 1205)
                {
                    throw new DeadLockException(ex.Message);
                }
                catch (SqlException ex) when (ex.Number == -2)
                {
                    throw new DatabaseTimeoutException(ex.Message);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}
