﻿using System.Linq;
using System.Threading.Tasks;

namespace Yudoo.Infrastructure.Repositories
{
    public interface ISellerRepositories
    {
        Task<bool> GetRegisteredShopDetails(string name, string shop_name, string mobile);
        Task AddUpdateShopOwner(int? id, string shop_name, string shop_owner_name, string bank_name, string bank_account_no,
            string ifsc_code, string account_holder_name, int? main_category_id, string address_line_1, string address_line_2, 
            string land_mark,int? pin_code, int? created_by, int? ratings, int? ratings_by);
    }
    public class SellerRepositories : BaseRepository,ISellerRepositories
    {
        public SellerRepositories(string connectionString) : base(connectionString)
        { 
            
        }


        public async Task<bool> GetRegisteredShopDetails(string name, string shop_name, string mobile)
        {
            var sql = "check_customer_exists";

            var result = await QueryAsync<int>(sql, new
            {
                customerName = name,
                shopName = shop_name,
                mobileNumber = mobile
            });

            return result.Any();
        }

        public async Task AddUpdateShopOwner(int? id, string shop_name, string shop_owner_name, string bank_name, string bank_account_no,
            string ifsc_code, string account_holder_name, int? main_category_id, string address_line_1, string address_line_2,
            string land_mark, int? pin_code, int? created_by, int? ratings, int? ratings_by)
        {
            var sql = "ShopOwner_insertUpdate";

            await ExecuteAsync(sql, new
            {
                Id = id,
                ShopName = shop_name,
                ShopOwnerName = shop_owner_name,
                BankName = bank_name,
                BankAccountNo = bank_account_no,
                IFSCCode = ifsc_code,
                AccountHolderName = account_holder_name,
                MovieCategoryId = main_category_id,
                AddressLine1 = address_line_1,
                AddressLine2 = address_line_2,
                LandMark = land_mark,
                PinCode = pin_code,
                CreatedBy = created_by,
                Ratings = ratings,
                RatingsBy = ratings_by
            });
        }
    }
}
